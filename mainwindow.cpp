#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDesktopServices>
#include <QFileDialog>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

#include "photolistwidget.h"

static const QString& AppVersion = "1.2";
static const QString& AppAuthor = "LK";
static const QString& AppWebSite = "<a href=\"http://my3soft.getenjoyment.net\">my3soft.getenjoyment.net</a>";

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QPushButton* btnRemove = new QPushButton("Remove");
    QPushButton* btnClear = new QPushButton("Clear");
    QPushButton* btnMerge = new QPushButton("Merge");
    QPushButton* btnSave = new QPushButton("Save...");

    QLayout* buttons = new QHBoxLayout();
    buttons->setSpacing(5);
    buttons->addWidget(btnRemove);
    buttons->addWidget(btnClear);
    buttons->addItem(new QSpacerItem(10, 10));
    buttons->addWidget(btnMerge);
    buttons->addItem(new QSpacerItem(10, 10));
    buttons->addWidget(btnSave);

    QLayout* layout = new QVBoxLayout();
    layout->addWidget(&photoList);
    layout->addItem(buttons);

    centralWidget()->setLayout(layout);

    connect(btnRemove, SIGNAL(clicked()), this, SLOT(Remove()));
    connect(btnClear, SIGNAL(clicked()), this, SLOT(Clear()));
    connect(btnMerge, SIGNAL(clicked()), this, SLOT(Merge()));
    connect(btnSave, SIGNAL(clicked()), this, SLOT(Save()));

    QLabel *statusLabel = new QLabel(this);
    statusLabel->setText(QString("ver. %1 GPLv3 powered by Qt %2, OpenCV %3, written by %4, site: %5")
                         .arg(AppVersion).arg(qVersion()).arg(CV_VERSION).arg(AppAuthor).arg(AppWebSite));
    statusLabel->setTextFormat(Qt::RichText);
    statusBar()->addWidget(statusLabel);

    connect(statusLabel, SIGNAL(linkActivated(QString)), this, SLOT(handleLinkActivated(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showPreview()
{
    preview.setImage(QImageFromCvMat(lastPhoto));
    preview.show();
}

void MainWindow::handleLinkActivated(const QString& link)
{
    QDesktopServices::openUrl(QUrl(link));
}

QImage MainWindow::QImageFromCvMat(cv::Mat& cvMat)
{
    cvtColor(cvMat, cvMat, cv::COLOR_BGR2RGB);
    return QImage((uchar*)cvMat.data, cvMat.cols, cvMat.rows, cvMat.step, QImage::Format_RGB888);
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    preview.hide();
}

void MainWindow::Remove()
{
    foreach (QListWidgetItem* lwi, photoList.selectedItems())
    {
        delete lwi;
    }
}

void MainWindow::Clear()
{
    photoList.clear();
}

void MainWindow::Merge()
{
    photoList.mergePhotos(lastPhoto);
    showPreview();
}

void MainWindow::Save()
{
    static QString lastDir = "";
    if (lastPhoto.empty())
    {
        return;
    }
    const QString& path = QFileDialog::getSaveFileName(this, "Save", lastDir, "Images (*.jpg)");
    if (path.isEmpty())
    {
        return;
    }
    lastDir = QFileInfo(path).dir().path();
    cv::imwrite(path.toStdString(), lastPhoto);
}
