#ifndef PHOTOLISTITEM_H
#define PHOTOLISTITEM_H

#include <QBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QSlider>
#include <QWidget>

#define PHOTO_LIST_THUMBNAIL_SIZE 100

class PhotoListItem : public QWidget
{
        Q_OBJECT

    public:
        PhotoListItem(const QIcon& icon, const QString& text, const int sliderValue,
                      QWidget* parent = nullptr);

        QPixmap getIcon() const;
        QString getText() const;
        QSlider* getSlider();

    private slots:
        void showSliderValue(int value);

    private:
        QLabel* iconLabel;
        QLabel* textLabel;
        QSlider* slider;
        QLabel* valueLabel;
};

#endif // PHOTOLISTITEM_H
