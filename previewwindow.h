#ifndef PREVIEWWINDOW_H
#define PREVIEWWINDOW_H

#include <QDialog>

class PreviewWindow : public QDialog
{
    public:
        PreviewWindow();

        void setImage(const QImage& image);

    protected:
        void paintEvent(QPaintEvent* e) override;

    private:
        QImage img;
};

#endif // PREVIEWWINDOW_H
