QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH += D:/opencv-4.5.3/build/install/include
LIBS += -L"D:/opencv-4.5.3/build/install/x64/mingw/lib" -lopencv_core453 -lopencv_highgui453 -lopencv_imgcodecs453 -lopencv_imgproc453

CONFIG += c++17

RC_ICONS = fusion.ico

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    photolistitem.cpp \
    photolistwidget.cpp \
    previewwindow.cpp

HEADERS += \
    mainwindow.h \
    photolistitem.h \
    photolistwidget.h \
    previewwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
