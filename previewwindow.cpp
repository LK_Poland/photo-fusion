#include "previewwindow.h"

#include <QApplication>
#include <QPainter>
#include <QScreen>

PreviewWindow::PreviewWindow()
{
    setWindowTitle("Preview");
}

void PreviewWindow::setImage(const QImage& image)
{
    const int width = std::min(
                          QApplication::primaryScreen()->geometry().width() / 3,
                          image.width());
    img = image.scaledToWidth(width);
    resize(img.size());;
    setMinimumSize(size());
    setMaximumSize(size());
    repaint();
}

void PreviewWindow::paintEvent(QPaintEvent* e)
{
    QPainter p(this);
    p.drawImage(rect(), img);
}
