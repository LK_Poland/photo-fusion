#ifndef PHOTOLISTWIDGET_H
#define PHOTOLISTWIDGET_H

#include <QDragEnterEvent>
#include <QFileInfo>
#include <QIcon>
#include <QListWidget>
#include <QMimeData>
#include <QUrl>

#include <opencv2/opencv.hpp>

#include "photolistitem.h"

class PhotoListWidget : public QListWidget
{
    Q_OBJECT

    public:
        PhotoListWidget(QWidget *parent = nullptr);

        void mergePhotos(cv::Mat& merged);

    private slots:
        void sliderValueChanged(int value);

    protected:
        void dragEnterEvent(QDragEnterEvent *event) override;
        void dragMoveEvent(QDragMoveEvent *event) override;
        void dropEvent(QDropEvent *event) override;
};

#endif // PHOTOLISTWIDGET_H
