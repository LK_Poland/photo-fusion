#include "photolistitem.h"

PhotoListItem::PhotoListItem(const QIcon& icon, const QString& text, const int sliderValue, QWidget* parent)
    : QWidget(parent)
{
    QHBoxLayout* layout = new QHBoxLayout(this);

    iconLabel = new QLabel(this);
    iconLabel->setPixmap(icon.pixmap(PHOTO_LIST_THUMBNAIL_SIZE, PHOTO_LIST_THUMBNAIL_SIZE));

    textLabel = new QLabel(text, this);
    valueLabel = new QLabel("", this);

    slider = new QSlider(Qt::Horizontal, this);
    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(showSliderValue(int)));
    slider->setRange(0, 100);
    slider->setValue(sliderValue);

    layout->addWidget(iconLabel);
    layout->addWidget(textLabel);
    layout->addWidget(slider);
    layout->addWidget(valueLabel);
}

QPixmap PhotoListItem::getIcon() const
{
    return iconLabel->pixmap(Qt::ReturnByValue);
}

QString PhotoListItem::getText() const
{
    return textLabel->text();
}

QSlider* PhotoListItem::getSlider()
{
    return slider;
}

void PhotoListItem::showSliderValue(int value)
{
    valueLabel->setText(QString("%1%").arg(value));
}
