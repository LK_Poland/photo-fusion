#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "photolistwidget.h"
#include "previewwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

        void showPreview();

        static QImage QImageFromCvMat(cv::Mat& cvMat);

    protected:
        void closeEvent(QCloseEvent* e) override;

    public slots:
        void Remove();
        void Clear();
        void Merge();
        void Save();

    private slots:
        void handleLinkActivated(const QString& link);

    private:
        Ui::MainWindow* ui;
        PhotoListWidget photoList;
        cv::Mat lastPhoto;
        PreviewWindow preview;
};
#endif // MAINWINDOW_H
