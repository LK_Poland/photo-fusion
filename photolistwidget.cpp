#include "photolistwidget.h"

PhotoListWidget::PhotoListWidget(QWidget *parent) : QListWidget(parent)
{
    setAcceptDrops(true);
    setDragDropMode(QAbstractItemView::DragDropMode::DropOnly);
    setIconSize(QSize(PHOTO_LIST_THUMBNAIL_SIZE, PHOTO_LIST_THUMBNAIL_SIZE));
    setSelectionMode(QAbstractItemView::ExtendedSelection);
}

void PhotoListWidget::mergePhotos(cv::Mat& merged)
{
    const int photoCount = count();
    if (!photoCount)
    {
        return;
    }
    int minWidth = INT_MAX;
    int minHeight = INT_MAX;
    QSlider* sliders[photoCount];
    cv::Mat photos[photoCount];
    cv::Mat result;
    for (int i = 0; i != photoCount; ++i)
    {
        QListWidgetItem* lwi = item(i);
        sliders[i] = ((PhotoListItem*)itemWidget(lwi))->getSlider();
        const QFileInfo& fileInfo = lwi->data(Qt::UserRole).value<QFileInfo>();
        cv::Mat& photo = photos[i];
        photo = cv::imread(fileInfo.filePath().toStdString());
        if (photo.cols < minWidth || photo.rows < minHeight)
        {
            minWidth = photo.cols;
            minHeight = photo.rows;
        }
    }
    for (int i = 0; i != photoCount; ++i)
    {
        const cv::Mat& photo = photos[i];
        cv::Mat resized;
        cv::resize(photo, resized, cv::Size(minWidth, minHeight));
        resized *= sliders[i]->value() / 100.0;
        if (result.empty())
        {
            result = resized;
        }
        else
        {
            result += resized;
        }
    }
    merged = result;
}

void PhotoListWidget::sliderValueChanged(int value)
{
    QSlider* sliders[count()];
    int total = 0;
    for (int i = 0; i != count(); ++i)
    {
        PhotoListItem* it = (PhotoListItem*)itemWidget(item(i));
        sliders[i] = it->getSlider();
        total += sliders[i]->value();
    }
    const int step = 1;
    while (total > 100)
    {
        int sub = 0;
        for (int i = 0; i != count(); ++i)
        {
            if (sender() != sliders[i] && sliders[i]->value() > 0)
            {
                sub += step;
                sliders[i]->setValue(sliders[i]->value() - step);
                if (total -= step <= 100)
                {
                    break;
                }
            }
        }
        if (!sub)
        {
            break;
        }
    }
}

void PhotoListWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls())
    {
        event->acceptProposedAction();
    }
}

void PhotoListWidget::dragMoveEvent(QDragMoveEvent *event)
{
    event->acceptProposedAction();
}

void PhotoListWidget::dropEvent(QDropEvent *event)
{
    const QMimeData* mimeData = event->mimeData();
    if (mimeData->hasUrls())
    {
        const QList<QUrl>& urls = mimeData->urls();
        for (const QUrl& url : urls)
        {
            const QString& filePath = url.toLocalFile();
            const QFileInfo& fileInfo = filePath;
            const QString& fileSuffix = fileInfo.suffix().toLower();
            if (fileInfo.isFile() &&
                (fileSuffix == "jpg" || fileSuffix == "jpeg"))
            {
                const QPixmap& pixmap = filePath;
                const double ratio = pixmap.width() / (double)pixmap.height();
                PhotoListItem* pli = new PhotoListItem(pixmap,
                                                       QString("%1 [%2x%3] [ratio=%4]")
                                                       .arg(fileInfo.fileName())
                                                       .arg(pixmap.width())
                                                       .arg(pixmap.height())
                                                       .arg(QString::number(ratio, 'f', 2)),
                                                       count() ? 0 : 100,
                                                       this);
                connect(pli->getSlider(), SIGNAL(valueChanged(int)),
                                                 this, SLOT(sliderValueChanged(int)));
                QListWidgetItem* lwi = new QListWidgetItem();
                lwi->setData(Qt::UserRole, QVariant::fromValue(fileInfo));
                lwi->setSizeHint(QSize(0, PHOTO_LIST_THUMBNAIL_SIZE));
                addItem(lwi);
                setItemWidget(lwi, pli);
            }
        }
        event->acceptProposedAction();
    }
}
